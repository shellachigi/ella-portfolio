<!-- Contact Section-->
<section class="page-section" id="contact">
    <div class="container">
        <!-- Contact Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Contact Section Form-->
        <div class="row d-flex justify-content-center">
            <div class="col-lg-4 ms-auto text-center">
                <h4 class="text-uppercase mb-4">Contact Number</h4>
                <p class="lead">+639568015003</p>
            </div>
            <div class="col-lg-4 me-auto text-center">
                <h4 class="text-uppercase mb-4">Email Address</h4>
                <p class="lead">shellachigi@gmail.com</p>
            </div>
            <div class="col-lg-4 me-auto text-center">
                <h4 class="text-uppercase mb-4">Location</h4>
                <p class="lead">3101 Brgy. 894 Punta, Sta. Ana, Metro Manila</p>
            </div>
        </div>
    </div>
</section>