<!-- Portfolio Section-->
<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Portfolio</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">
            <!-- Portfolio Item 1-->
            <div class="col-md-6 col-lg-4 mb-5">
                <a class="portfolio-item mx-auto" href="https://dreamersrocket.com/">
                    <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-search fa-3x"></i></div>
                    </div>
                    <img class="img-fluid" src="{{ url('assets/img/portfolio/dreamersrocket.png') }}" alt="..." />
                    <p class="lead text-center pt-5">DreamersRocket</p>
                </a>
            </div>
            <!-- Portfolio Item 2-->
            <div class="col-md-6 col-lg-4 mb-5">
                <a class="portfolio-item mx-auto" href="http://demo-account.sip-lite.com/">
                    <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-search fa-3x"></i></div>
                    </div>
                    <img class="img-fluid" src="{{ url('assets/img/portfolio/reward-system.png') }}" alt="..." />
                    <p class="lead text-center pt-5">Rewards Incentives Technology</p>
                </a>
            </div>
           
        </div>
    </div>
</section>