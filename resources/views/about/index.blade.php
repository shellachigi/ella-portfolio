<!-- About Section-->
<section class="page-section bg-primary text-white mb-0" id="about">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
        <!-- Icon Divider-->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row">
            <div class="col-lg-4 ms-auto">
                <p class="lead">I'm Shella Marie M. Diaz or you can call me "Ella" a Web Developer based in Sta. Ana, Metro Manila, Philippines.</p>
                <p class="lead">My coding journey started last 2021 where I enjoy developing user interface and currently exploring, studying and enhancing my skills in backend.</p>
            </div>
            <div class="col-lg-4 me-auto">
                <p class="lead"><strong>Development: </strong>HTML, CSS, Javascript, ReactJS, ExpressJS, NodeJS,WordPress, Laravel, Responsive Web Design</p>
                <p class="lead"><b>Tools: </b>VS Code, GIT, HeidiSQL, MongoDB, Postman</p>
                <p class="lead"><b>Server: </b>AWS</p>
            </div>
        </div>
        <!-- About Section Button-->
        {{-- <div class="text-center mt-4">
            <a class="btn btn-xl btn-outline-light" href="https://startbootstrap.com/theme/freelancer/">
                <i class="fas fa-download me-2"></i>
                Free Download!
            </a>
        </div> --}}
    </div>
</section>